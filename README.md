# Laravel - Your Energy Usage

To view the project live: www.romin.dev

You can find the relevant code in the following directories:

## Models

```
/app
```

## Controllers

```
/app/Http/Controllers
```

## Views / HTML

```
/views
```

## CSS/SASS

```
/resources/sass
```

## JS

```
/resources/js/app.js
```

## Database and Seeders

```
/database
```

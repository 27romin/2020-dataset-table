<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnergyDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('energy_data', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('site_name');
            $table->string('utility');
            $table->string('month');
            $table->integer('budget_cost');
            $table->integer('budget_usage');
            $table->integer('days_invoiced');
            $table->integer('estimated_fixed_cost');
            $table->integer('estimated_usage');
            $table->integer('estimated_variable_cost');
            $table->integer('invoiced_cost');
            $table->integer('invoiced_usage');
            $table->integer('uninvoiced_cost');
            $table->integer('uninvoiced_usage');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('energy_data');
    }
}

<?php

namespace App\Http\Controllers;

use Storage;
use App\EnergyData;
use League\Csv\Reader;
use League\Csv\Statement;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Get records from database.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hasFile = Storage::disk('public')->has('data.csv');
        $records = EnergyData::paginate();
        $search = request()->get('search');

        if($search) {
            $records = EnergyData::where('site_name', 'like', '%' . $search . '%')->paginate();
            $records->appends(compact('search'));
        }

        return view('home.index', compact('records', 'hasFile'));
    }
}

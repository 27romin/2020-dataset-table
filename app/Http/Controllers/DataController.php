<?php

namespace App\Http\Controllers;

use Storage;
use App\EnergyData;
use League\Csv\Reader;
use League\Csv\Statement;
use Illuminate\Http\Request;

class DataController extends Controller
{

    /**
     * Upload the dataset and save to database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'file' => 'required|max:10000|mimes:csv,txt'
        ]);

        $request->file->storeAs('public', 'data.csv');

        $file = Reader::createFromPath(public_path() . '/storage/data.csv', 'r')
        ->setHeaderOffset(0);

        $stmt = new Statement();
        $records = $stmt->process($file);
        $records = collect($records);

        foreach($records as $record) {
            EnergyData::create([
                'month' => $record['Month'],
                'utility' => $record['Utility'],
                'site_name' => $record['SITE_NAME'],
                'budget_cost' => $record['BUDGET_COST'],
                'budget_usage' => $record['BUDGET_USAGE'],
                'days_invoiced' => $record['DAYS_INVOICED'],
                'estimated_usage' => $record['ESTIMATED_USAGE'],
                'invoiced_cost' => $record['INVOICED_COST'],
                'invoiced_usage' => $record['INVOICED_USAGE'],
                'uninvoiced_cost' => $record['UNINVOICED_COST'],
                'uninvoiced_usage' => $record['UNINVOICED_USAGE'],
                'estimated_fixed_cost' => $record['ESTIMATED_FIXED_COST'],
                'estimated_variable_cost' => $record['ESTIMATED_VARIABLE_COST'],
            ]);
        }

        return redirect()->route('home.index')->withSuccess('File Uploaded.');
    }

    /**
     * Delete the CSV file and empty table.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        Storage::disk('public')->delete('data.csv');
        EnergyData::truncate();

        return redirect()->route('home.index')->withSuccess('File Removed.');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnergyData extends Model
{
    protected $guarded = ['id'];
}

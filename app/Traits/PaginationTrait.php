<?php
 
namespace App\Traits;
 
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
 
trait PaginationTrait {
 
    /**
     * Custom method to paginate an array 
     * of records.
     *
     * @param  int  $records
     * @param  int  $perPage
     * @return array \Illuminate\Pagination\LengthAwarePaginator
     */
    public function paginate($records, $perPage)
    {
        $data = Collection::make($records);
        $page = Paginator::resolveCurrentPage();

        if(!$page) {
            $page = 1;
        }

        return new LengthAwarePaginator(
            $data->forPage($page, $perPage), 
            $data->count(), 
            $perPage, 
            $page
        );
    }
}
// require('./bootstrap');

import $ from 'jquery';

$(() => {
    $('.toggle-data').on('click', function() {
        let id = $(this).attr('data-id');

        $(this).toggleClass('data__icon--active');
        $('i', this).toggleClass('fa-arrow-alt-circle-up');
        $('i', this).toggleClass('fa-arrow-alt-circle-down');
        $('#data-' + id).toggleClass('data--toggle');
    });
});
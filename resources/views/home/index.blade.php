@extends('layouts.default')

@section('content')

    <h1>Your Energy Usage</h1>

    @if(!$hasFile)
        @include('partials.errors')

        <form action="{{ route('data.store') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="file" name="file" />
            <input type="submit" value="Upload" class="button button--primary" />
        </form>
    @endif

    @if($hasFile)
        <form action="{{ route('data.destroy') }}" method="POST">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <button type="submit" class="button button--danger">Import .CSV</button>
        </form>

        @if(request()->get('search'))
            <p>
                <a class="button button--small button--warning" href="{{ route('home.index') }}">
                Clear Search: {{ request()->get('search') }}
                </a>
            </p>
        @endif

        <form method="GET" role="search">
            <input type="text" class="form-control" name="search" placeholder="Site Name">
            <button type="submit" class="button button--primary">
                Search
            </button>
        </form>
    @endif

    @if(count($records))
        {{ $records->links() }}
    @endif

    @include('partials.success')

    <table>
        <tr>
            <th>Site Name</th>
            <th>Utility</th>
            <th>Month</th>
            <th></th>
        </tr>
        @forelse($records as $record)
            <tr>
                <td width="60%">{{ $record->site_name }}</td>
                <td>{{ $record->utility }}</td>
                <td width="30%">{{ $record->month }}</td>
                <td>
                    <span class="toggle-data data__icon" data-id="{{ $record->id }}">
                        <i class="far fa-arrow-alt-circle-up"></i>
                    </span>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="padding--none">
                    <div class="data" id="data-{{ $record->id }}">
                        <div class="grid">
                            <div class="grid__col-2">
                                <div class="padding--1">
                                    <ul>
                                        <li><strong>Budget Cost:</strong> @money($record->budget_cost)</li>
                                        <li><strong>Budget Usage:</strong> @money($record->budget_usage)</li>
                                    </ul>

                                    <ul>
                                        <li><strong>Estimated Fixed Cost:</strong> @money($record->estimated_fixed_cost)</li>
                                        <li><strong>Estimated Usage:</strong> @money($record->estimated_usage)</li>
                                        <li><strong>Estimated Variable Cost:</strong> @money($record->estimated_variable_cost)</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="grid__col-2">
                                <div class="padding--1">
                                    <ul>
                                        <li><strong>Invoiced Cost:</strong> @money($record->invoiced_cost)</li>
                                        <li><strong>Invoiced Usage:</strong> @money($record->invoiced_usage)</li>
                                    </ul>

                                    <ul>
                                        <li><strong>Uninvoiced Cost:</strong> @money($record->uninvoiced_cost)</li>
                                        <li><strong>Uninvoiced Usage:</strong> @money($record->uninvoiced_usage)</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        @empty 
            <tr>
                <td colspan="4">No Records</td>
            </tr>
        @endforelse
    </table>
    
    @if(count($records))
        {{ $records->links() }}
    @endif

@endsection
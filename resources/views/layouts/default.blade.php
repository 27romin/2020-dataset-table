<html>
    <head>
        <title>Your Energy Usage</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://kit.fontawesome.com/1d83160788.js" crossorigin="anonymous"></script>
        <link href="{{ asset('/css/app.css') }}" rel="stylesheet"> 
    </head>
    <body>
        <div class="container">
            @yield('content')
        </div>
    </body>

    <script src="{{ asset('js/app.js')}}"></script>
</html>